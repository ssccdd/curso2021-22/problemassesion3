/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package es.uja.ssccdd.curso2122.problemassesion3.grupo4;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import static es.uja.ssccdd.curso2122.problemassesion3.grupo4.Utils.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Phaser;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class PlataformaAprendizaje implements Runnable {

    private final int iD;
    private final ArrayList<PlanEstudios> planes;
    private final ArrayList<Recurso> recursosPendientes;
    private final ArrayList<Recurso> recursosNoAsignados;
    private final CountDownLatch sincCreacionRecursos;
    private final Phaser sincEntrePlataformas;
    private boolean interrumpido;
    private int recursosProcesados;
    private int planesCompletados;

    public PlataformaAprendizaje(int iD, ArrayList<PlanEstudios> planes, ArrayList<Recurso> recursosPendientes, ArrayList<Recurso> recursosNoAsignados, CountDownLatch sincCreacionRecursos, Phaser sincEntrePlataformas) {
        this.iD = iD;
        this.planes = planes;
        this.recursosPendientes = recursosPendientes;
        this.recursosNoAsignados = recursosNoAsignados;
        this.sincCreacionRecursos = sincCreacionRecursos;
        this.sincEntrePlataformas = sincEntrePlataformas;
        this.interrumpido = false;
        this.recursosProcesados = 0;
        this.planesCompletados = 0;
    }

    @Override
    public void run() {

        System.out.println(Thread.currentThread().getName() + " ha comenzado, esperando a que termine la generación de recursos.");

        try {
            sincCreacionRecursos.await();
            System.out.println(Thread.currentThread().getName() + " terminada la generación, empezando organización.");

            for (int i = 0; i < recursosPendientes.size() && !interrumpido; i++) {

                //Se prueba a insertar un recurso en los planes disponibles
                int planInsertado = organizarRecurso(i);

                //En cualquier caso, se marca como procesado
                recursosProcesados++;

                try {
                    TimeUnit.MILLISECONDS.sleep(random.nextInt(TIEMPO_ASIGNACION_RECURSOS_MAX - TIEMPO_ASIGNACION_RECURSOS_MIN) + TIEMPO_ASIGNACION_RECURSOS_MIN);
                } catch (InterruptedException ex) {
                    //Si nos interrupten en la espera del último recurso, no lo contamos.
                    if (recursosPendientes.size() != recursosProcesados) {
                        interrumpido = true;
                    }
                }
                try {
                    if (planInsertado >= 0) {//Ha sido insertado
                        if (planes.get(planInsertado).isCompleto()) {//He completado un plan
                            planesCompletados++;
                            System.out.println(Thread.currentThread().getName() + " ha completado el plan " + planesCompletados + " y esperará al resto de las plataformas");
                            sincEntrePlataformas.awaitAdvanceInterruptibly(sincEntrePlataformas.arrive());
                        }
                    }
                } catch (InterruptedException ex) {
                    interrumpido = true;
                }

            }
        } catch (InterruptedException ex) {
            System.out.println("WARNING: " + Thread.currentThread().getName() + " ha sido interrumpido mientras esperaba a las universidades.");
        }

        sincEntrePlataformas.arriveAndDeregister();// El hilo ha terminado

        System.out.println(this.toString());

    }

    /**
     * Intenta introducir un recurso en algún plan de estudios
     *
     * @param indiceRecurso indice del recurso a insertar
     * @return true si ha podido ser insertado
     */
    private int organizarRecurso(int indiceRecurso) {

        int insertado = -1;
        for (int j = 0; j < planes.size() && insertado < 0; j++) {
            if (planes.get(j).addRecurso(recursosPendientes.get(indiceRecurso))) {
                insertado = j;
            }
        }

        //Si no se ha podido insertar se guarda la incidencia
        if (insertado < 0) {
            recursosNoAsignados.add(recursosPendientes.get(indiceRecurso));
        }

        return insertado;

    }

    /**
     * Calcula el porcentaje de trabajo realizado
     *
     * @return procentaje de trabajo realizado
     */
    private float porcentajeCompletado() {
        return 1.0f * recursosProcesados / recursosPendientes.size();
    }

    /**
     * Devuelve una cadena con la información de la clase
     *
     * @return Cadena de texto con la información
     */
    @Override
    public String toString() {
        StringBuilder mensaje = new StringBuilder();
        mensaje.append(Thread.currentThread().getName()).append(" ");

        if (interrumpido) {
            mensaje.append("\"INTERRUMPIDO\" en la fase ").append(planesCompletados);
        }

        mensaje.append(", ").append(recursosPendientes.size()).append(" recursos disponibles, de los cuales se han procesado el ").append(porcentajeCompletado() * 100).append("%.");
        mensaje.append("\n Planes disponibles ").append(planes.size()).append(", planes completados ").append(planesCompletados).append(".");

        for (PlanEstudios plan : planes) {
            mensaje.append("\n\t").append(plan.toString());
        }

        if (recursosProcesados < recursosPendientes.size()) {
            mensaje.append("\n\tSe han quedado fuera ").append(recursosPendientes.size() - recursosProcesados).append(" recursos por falta de tiempo.");
        }

        return mensaje.toString();
    }

}
