/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2122.problemassesion3.grupo3;

import static es.uja.ssccdd.curso2122.problemassesion3.grupo3.Constantes.MIN_COMPONENTES;
import static es.uja.ssccdd.curso2122.problemassesion3.grupo3.Constantes.MIN_TIEMPO_FABRICACION;
import es.uja.ssccdd.curso2122.problemassesion3.grupo3.Constantes.TipoComponente;
import static es.uja.ssccdd.curso2122.problemassesion3.grupo3.Constantes.VARIACION_COMPONENTES;
import static es.uja.ssccdd.curso2122.problemassesion3.grupo3.Constantes.VARIACION_TIEMPO_FABRICACION;
import static es.uja.ssccdd.curso2122.problemassesion3.grupo3.Constantes.aleatorio;
import java.util.ArrayList;
import java.util.concurrent.Phaser;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pedroj
 */
public class Fabricante implements Runnable {
    private final String iD;
    private final ArrayList<Componente> listaComponentes;
    private final TipoComponente componente;
    private final Phaser finFabricacion;

    public Fabricante(String iD, ArrayList<Componente> listaComponentes, TipoComponente componente, Phaser finFabricacion) {
        this.iD = iD;
        this.listaComponentes = listaComponentes;
        this.componente = componente;
        this.finFabricacion = finFabricacion;
        
        // Se añade al punto de sincronización para indicar que ha finalizado
        this.finFabricacion.register();
    }
    
    @Override
    public void run() {
        int numeroComponentes;
        
        System.out.println("El HILO(" + iD + ") comienza su ejecución");
        
        try {
            
            numeroComponentes = fabricarComponentes();
            
            System.out.println("El HILO(" + iD + ") ha fabricado " + numeroComponentes +
                                " " + componente);
        } catch(InterruptedException ex) {
            System.out.println("El HILO(" + iD + ") ha sido INTERRUMPIDO");      
        } finally {
            finFabricacion();
        }
    }

    public String getiD() {
        return iD;
    }
    
    private int fabricarComponentes() throws InterruptedException {
        int tiempoFabricacion;
        int numeroComponentes;
        
        numeroComponentes = MIN_COMPONENTES + aleatorio.nextInt(VARIACION_COMPONENTES);
        for(int i = 0; i < numeroComponentes; i++) {
            try {
                listaComponentes.add(new Componente(iD + "-" + this.hashCode(), componente));
            } catch(IndexOutOfBoundsException ex) {
                // Un error de acceso concurrente a la estructura de datos
                // damos por finalizada la tarea
                throw new InterruptedException("Error de acceso a la lista de componentes");
            }
            
            // Simula el tiempo de fabricación
            tiempoFabricacion = MIN_TIEMPO_FABRICACION + aleatorio.nextInt(VARIACION_TIEMPO_FABRICACION);
            TimeUnit.SECONDS.sleep(tiempoFabricacion);
        }
        
        return numeroComponentes;
    }
    
    /**
     * Acciones necesarias para indicar que se ha finalizado la fabricación
     * de los componentes al Proveedor al que está asociado.
     */
    private void finFabricacion() {
        this.finFabricacion.arriveAndDeregister();
    }
}
