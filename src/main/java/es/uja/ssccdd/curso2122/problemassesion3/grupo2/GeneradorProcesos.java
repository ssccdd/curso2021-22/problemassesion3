/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2122.problemassesion3.grupo2;

import static es.uja.ssccdd.curso2122.problemassesion3.grupo2.Constantes.MIN_PROCESOS;
import static es.uja.ssccdd.curso2122.problemassesion3.grupo2.Constantes.MIN_TIEMPO_CREACION;
import es.uja.ssccdd.curso2122.problemassesion3.grupo2.Constantes.TipoProceso;
import static es.uja.ssccdd.curso2122.problemassesion3.grupo2.Constantes.VARIACION_PROCESOS;
import static es.uja.ssccdd.curso2122.problemassesion3.grupo2.Constantes.VARIACION_TIEMPO_CREACION;
import static es.uja.ssccdd.curso2122.problemassesion3.grupo2.Constantes.aleatorio;
import java.util.ArrayList;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pedroj
 */
public class GeneradorProcesos implements Runnable {
    private final String iD;
    private final ArrayList<Proceso> listaProcesos;
    private final CyclicBarrier inicioGeneracion;
    private final CyclicBarrier finGeneracion;
    private int idProceso;

    public GeneradorProcesos(String iD, ArrayList<Proceso> listaProcesos, 
                             CyclicBarrier inicioGeneracion, CyclicBarrier finGeneracion) {
        this.iD = iD;
        this.listaProcesos = listaProcesos;
        this.inicioGeneracion = inicioGeneracion;
        this.finGeneracion = finGeneracion;
        this.idProceso = 0;
    }
    
    @Override
    public void run() {
        System.out.println("El HILO(" + iD + ") comienza su ejecución");
        
        try {
            while(true) {
                crearProcesos();
            }
        } catch(InterruptedException | BrokenBarrierException ex) {
            System.out.println("El HILO(" + iD + ") ha FINALIZADO");      
        }
    }

    public String getiD() {
        return iD;
    }
    
    private void crearProcesos() throws InterruptedException, BrokenBarrierException {
        Proceso proceso;
        int numProcesos;
        
        // Espera a que se le soliciten más procesos
        inicioGeneracion.await();
        
        numProcesos = MIN_PROCESOS + aleatorio.nextInt(VARIACION_PROCESOS);
        for(int i = 0; i < numProcesos; i++) {
            // Simulamos el tiempo de operación
            TimeUnit.SECONDS.sleep(MIN_TIEMPO_CREACION + aleatorio.nextInt(VARIACION_TIEMPO_CREACION));
            
            proceso = new Proceso("Proceso-" + iD + "-" + idProceso, 
                                  TipoProceso.getTipoProceso());
     
            idProceso++;
            try {
                listaProcesos.add(proceso);
            } catch(IndexOutOfBoundsException ex) {
                // Un error de acceso concurrente a la estructura de datos
                // damos por finalizada la tarea
                throw new InterruptedException("Error de acceso a la lista de procesos");
            }
        }
        
        // Indica a su gestor que tiene procesos disponibles
        finGeneracion.await();
        System.out.println("El HILO(" + iD + ") ha generado " + numProcesos + " procesos");
    }
}
