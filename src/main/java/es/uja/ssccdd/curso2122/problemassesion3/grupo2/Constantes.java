/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2122.problemassesion3.grupo2;

import java.util.Random;

/**
 *
 * @author pedroj
 */
public interface Constantes {
    // Generador aleatorio
    public static final Random aleatorio = new Random();
    
    public enum TipoProceso {
        TIEMPO_REAL(20,2), INTERACTIVO(40,5), BATCH(60,8), SERVICIO(80,3), NOTIFICACION(100,1);
        
        private final int valor;
        private final int maximoPaginas;

        private TipoProceso(int valor, int maximoPaginas) {
            this.valor = valor;
            this.maximoPaginas = maximoPaginas;
        }
        
        /**
         * Genera aleatoriamente uno de los tipos de proceso disponibles
         * @return el tipo de proceso generado
         */
        public static TipoProceso getTipoProceso() {
            TipoProceso resultado = null;
            int valor = aleatorio.nextInt(D100);
            
            int i = 0;
            
            while( (i < procesos.length) && (resultado == null) ) {
                if( procesos[i].valor > valor)
                    resultado = procesos[i];
                
                i++;
            }
            return resultado;
        }

        /**
         * Indica el maximoPaginas para ese tipo de proceso
         * @return el maximoPaginas
         */
        public int getMaximoPaginas() {
            return maximoPaginas;
        }
    }
    
    // Constantes de la sesión
    public static final int D100 = 100; // Tirada de dato de 100 caras
    public static final TipoProceso[] procesos = TipoProceso.values();
    public static final boolean ASIGNADO = true;
    public static final int PRIMERO = 0;
    public static final int NINGUNO = 0;
    public static final int MIN_PROCESOS = 3;
    public static final int VARIACION_PROCESOS = 3;
    public static final int MIN_TIEMPO_CREACION = 1;
    public static final int VARIACION_TIEMPO_CREACION = 2;
    public static final int ESPERAR_GENERACION = 1; // Un generador ha terminado
    public static final int FIN_GESTORES = 1; // Para esperar a que un gertor finalice
    public static final int MIN_TIEMPO_PROCESADO = 2;
    public static final int VARIACION_TIEMPO_PROCESADO = 3;
    public static final int NUM_GESTORES = 3;
    public static final int MIN_GENERADORES = 2;
    public static final int VARIACION_GENERADORES = 2;
    public static final int MIN_PAGINAS = 50;
    public static final int VARIACION_PAGINAS = 21;
    public static final int MIN_PROCESOS_PARA_ASIGNAR = 5;
    public static final int VARIACION_ASIGNACION_PROCESOS = 6;
}
