/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2122.problemassesion3.grupo6;

import static es.uja.ssccdd.curso2122.problemassesion3.grupo6.Utils.*;
import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Phaser;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Sesion3 {

    public static void main(String[] args) {
        // Variables aplicación
        int idEquipos = 0;
        int idJugadores = 0;
        int idOrganizadores = 0;

        ArrayList<Thread> hilosOrganizadores = new ArrayList<>();
        ArrayList<Thread> hilosRegistradores = new ArrayList<>();
        ArrayList<Jugador> jugadoresPerdidos = new ArrayList<>();
        Phaser sincOrganizadores = new Phaser(ORGANIZADORES_A_GENERAR);

        // Ejecución del hilo principal
        System.out.println("HILO-Principal Ha iniciado la ejecución");

        for (int i = 0; i < ORGANIZADORES_A_GENERAR; i++) {

            // Inicializamos los equipos
            ArrayList<Equipo> listaEquipos = new ArrayList<>();
            for (int j = 0; j < EQUIPOS_A_GENERAR; j++) {
                listaEquipos.add(new Equipo(idEquipos++));
            }

            // Inicializamos los jugadores
            ArrayList<Jugador> listaJugadores = new ArrayList<>();

            // Inicializamos los registros 
            CountDownLatch sincRegistro = new CountDownLatch(JUGADORES_A_GENERAR);
            RegistroJugadores registro = new RegistroJugadores(i, listaJugadores, sincRegistro);
            Thread threadRegistro = new Thread(registro, "Registro " + i);
            threadRegistro.start();
            hilosRegistradores.add(threadRegistro);

            // Inicializamos los organizadores
            Organizador organizador = new Organizador(idOrganizadores++, listaEquipos, listaJugadores, jugadoresPerdidos, sincRegistro, sincOrganizadores);
            Thread threadOrganizador = new Thread(organizador, "Organizador " + i);
            threadOrganizador.start();
            hilosOrganizadores.add(threadOrganizador);

        }

        System.out.println("HILO-Principal Espera a la finalización de los organizadores.");

        try {
            TimeUnit.SECONDS.sleep(TIEMPO_ESPERA_HILO_PRINCIPAL);
        } catch (InterruptedException ex) {
            Logger.getLogger(Sesion3.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.println("HILO-Principal Solicita la finalización de los organizadores.");

        for (int i = 0; i < ORGANIZADORES_A_GENERAR; i++) {
            hilosOrganizadores.get(i).interrupt();
        }

        System.out.println("HILO-Principal Espera a los organizadores.");

        for (int i = 0; i < ORGANIZADORES_A_GENERAR; i++) {
            try {
                hilosOrganizadores.get(i).join();
            } catch (InterruptedException ex) {
                Logger.getLogger(Sesion3.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        System.out.println("\nLos siguientes " + jugadoresPerdidos.size() + " jugadores no han podido ser asignados:");
        for (Jugador p : jugadoresPerdidos) {
            System.out.println("\t" + p);
        }

        System.out.println("HILO-Principal Ha finalizado la ejecución");
    }
}
