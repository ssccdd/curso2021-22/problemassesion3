/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package es.uja.ssccdd.curso2122.problemassesion3.grupo5;

import static es.uja.ssccdd.curso2122.problemassesion3.grupo5.Utils.*;
import java.util.ArrayList;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Deposito implements Runnable {

    private final int iD;
    private final ArrayList<Coche> colaCoches;
    private final CyclicBarrier sincPreparacion;
    private final CyclicBarrier sincAsignacion;
    private int faseInterrumpida;

    public Deposito(int iD, ArrayList<Coche> colaCoches, CyclicBarrier sincPreparacion, CyclicBarrier sincAsignacion) {
        this.iD = iD;
        this.colaCoches = colaCoches;
        this.sincPreparacion = sincPreparacion;
        this.sincAsignacion = sincAsignacion;
        faseInterrumpida = 0;
    }

    @Override
    public void run() {

        System.out.println(Thread.currentThread().getName() + " ha comenzado.");

        int identificador = 0;
        try {
            while (true) {
                faseInterrumpida = 0;
                TimeUnit.MILLISECONDS.sleep(Utils.random.nextInt(TIEMPO_ESPERA_PREPARACION_MAX - TIEMPO_ESPERA_PREPARACION_MIN) + TIEMPO_ESPERA_PREPARACION_MIN);
                int aleatorioCalidad = Utils.random.nextInt(VALOR_GENERACION);
                colaCoches.add(new Coche(identificador++, Utils.TipoReserva.getTipoReserva(aleatorioCalidad)));

                System.out.println(Thread.currentThread().getName() + " ha preparado un coche, esperando a que terminen el resto de depósitos.");
                faseInterrumpida = 1;
                sincPreparacion.await();

                System.out.println(Thread.currentThread().getName() + " todos los coches preparados, esperando a que terminen la asignación los gestores.");
                faseInterrumpida = 2;
                sincAsignacion.await();
                System.out.println(Thread.currentThread().getName() + " todos los coches asignados, iniciando la preparación de otro coche.");

            }
        } catch (Exception ex) {
            //Esta presvisto que se interrumpa y no debe hacer nada.
        }
        System.out.println(toString());
    }

    /**
     * Devuelve una cadena con la información de la clase
     *
     * @return Cadena de texto con la información
     */
    @Override
    public String toString() {
        StringBuilder mensaje = new StringBuilder();
        mensaje.append(Thread.currentThread().getName()).append(" ");

        mensaje.append("\"INTERRUMPIDO\" ").append(faseInterrumpida==0 ? "preparando" :(faseInterrumpida==1?"esperando resto de depósitos" : "esperando la asignación"));

        mensaje.append("\n Coches preparados:  ").append(colaCoches.size()).append(": ");

        for (Coche coche : colaCoches) {
            mensaje.append("\n\t").append(coche.toString());
        }

        return mensaje.toString();
    }

}
