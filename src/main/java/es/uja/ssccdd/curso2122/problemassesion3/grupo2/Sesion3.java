/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2122.problemassesion3.grupo2;

import static es.uja.ssccdd.curso2122.problemassesion3.grupo2.Constantes.ESPERAR_GENERACION;
import static es.uja.ssccdd.curso2122.problemassesion3.grupo2.Constantes.MIN_GENERADORES;
import static es.uja.ssccdd.curso2122.problemassesion3.grupo2.Constantes.MIN_PAGINAS;
import static es.uja.ssccdd.curso2122.problemassesion3.grupo2.Constantes.NUM_GESTORES;
import static es.uja.ssccdd.curso2122.problemassesion3.grupo2.Constantes.VARIACION_GENERADORES;
import static es.uja.ssccdd.curso2122.problemassesion3.grupo2.Constantes.VARIACION_PAGINAS;
import static es.uja.ssccdd.curso2122.problemassesion3.grupo2.Constantes.aleatorio;
import java.util.ArrayList;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import static es.uja.ssccdd.curso2122.problemassesion3.grupo2.Constantes.FIN_GESTORES;

/**
 *
 * @author UJA
 */
public class Sesion3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException, BrokenBarrierException {
        ArrayList<Thread>[] listaHilos;
        GestorProcesos gestor;
        ArrayList<Proceso>[] listaProcesos;
        ArrayList<Proceso>[] listaProcesosNoAsignados;
        RegistroMemoria[] listaResultados;
        CyclicBarrier[] inicioGeneracion;
        CyclicBarrier[] finGeneracion;
        CyclicBarrier finEjecucion;
        GeneradorProcesos generadorProcesos;
        
        // Ejecución del hilo principal
        System.out.println("Ha iniciado la ejecución el Hilo(PRINCIPAL)");
        
        // Inicialización de variables
        listaProcesos = new ArrayList[NUM_GESTORES];
        listaProcesosNoAsignados = new ArrayList[NUM_GESTORES];
        listaResultados = new RegistroMemoria[NUM_GESTORES];
        listaHilos = new ArrayList[NUM_GESTORES];
        inicioGeneracion = new CyclicBarrier[NUM_GESTORES];
        finGeneracion = new CyclicBarrier[NUM_GESTORES];
        finEjecucion = new CyclicBarrier(FIN_GESTORES + 1);
        for(int i = 0; i < NUM_GESTORES; i++) {
            int generadores = MIN_GENERADORES + aleatorio.nextInt(VARIACION_GENERADORES);
            int paginas = MIN_PAGINAS + aleatorio.nextInt(VARIACION_PAGINAS);
            listaResultados[i] = new RegistroMemoria("Registro-" + i, paginas);
            inicioGeneracion[i] = new CyclicBarrier(generadores + 1);
            finGeneracion[i] = new CyclicBarrier(generadores + 1);
            listaProcesos[i] = new ArrayList();
            listaProcesosNoAsignados[i] = new ArrayList();
            listaHilos[i] = new ArrayList();
            gestor = new GestorProcesos("Gestor-" + i, listaProcesos[i], listaResultados[i],
                                        listaProcesosNoAsignados[i], inicioGeneracion[i],
                                        finGeneracion[i], finEjecucion);
            listaHilos[i].add(new Thread(gestor));
            for(int j = 0; j < generadores; j++) {
                generadorProcesos = new GeneradorProcesos("Generador-" + i + "-" + j,
                                        listaProcesos[i], inicioGeneracion[i], finGeneracion[i]);
                listaHilos[i].add(new Thread(generadorProcesos));
            }
        }
        
        // Cuerpo de ejecución del hilo principal
        System.out.println("Hilo(PRINCIPAL) comienza la ejecución de los hilos");
        for(ArrayList<Thread> lista : listaHilos)
            for(Thread hilo : lista)
                hilo.start();
        
        System.out.println("Hilo(PRINCIPAL) espera a que finalice un gestor");
        finEjecucion.await();
        
        System.out.println("Hilo(PRINCIPAL) solicita la INTERRUPCION de los gestores y espera a su finalización");
        for(ArrayList<Thread> lista : listaHilos)
            for(Thread hilo : lista)
                hilo.interrupt();
        
        for(ArrayList<Thread> lista : listaHilos)
            for(Thread hilo : lista)
                hilo.join();
        
        // Presentar resultados
        for(int i = 0; i < NUM_GESTORES; i++) {
            System.out.println("_______ Resultados Gestor " + i + " _____________");
            System.out.println(listaResultados[i]);
            System.out.println(listaProcesosNoAsignados[i]);
            System.out.println("_________________________________________________");
        }
        
        // Finalización
        System.out.println("Ha finalizado la ejecución el Hilo(PRINCIPAL)");
    }
    
}
