﻿[![logo](https://www.gnu.org/graphics/gplv3-127x51.png)](https://choosealicense.com/licenses/gpl-3.0/)
# Problemas Prácticas
## Sesión 3
Problemas propuestos para la Sesión 3 de prácticas de la asignatura de Sistemas Concurrentes y Distribuidos del Grado en Ingeniería Informática de la Universidad de Jaén en el curso 2020-21.
Los ejercicios son diferentes para cada grupo:

Los **objetivos** de la práctica son:
- Definir adecuadamente las clases que implementan lo que se ejcutará en los _hilos_. Programando adecuadamente la excepción de interrupción de su ejecución.
- Utilizar adecuadamente la herramienta de sincronización propuesta para la resolución del ejercicio. Solo estará permitido utilizar objetos de esa herramienta y ningún otro.
- Modificar el método `main(..)` de la aplicación Java para una prueba correcta de las clases previamente definidas.
	 - Crear los objetos que representan lo que deberá ejecutarse en los _hilos_.
	 - Crear los objetos de los _hilos_ de ejecución. Asociando correctamente lo que deberá ejecutarse.
	 - La interrupción de los _hilos_ cuando sea solicitado.

Los ejercicios son diferentes para cada grupo:
-   [Grupo 1](https://gitlab.com/ssccdd/curso2021-22/problemassesion3/-/blob/master/README.md#grupo-1)
-   [Grupo 2](https://gitlab.com/ssccdd/curso2021-22/problemassesion3/-/blob/master/README.md#grupo-2)
-   [Grupo 3](https://gitlab.com/ssccdd/curso2021-22/problemassesion3/-/blob/master/README.md#grupo-3)
-   [Grupo 4](https://gitlab.com/ssccdd/curso2021-22/problemassesion3/-/blob/master/README.md#grupo-4)
-   [Grupo 5](https://gitlab.com/ssccdd/curso2021-22/problemassesion3/-/blob/master/README.md#grupo-5)
-   [Grupo 6](https://gitlab.com/ssccdd/curso2021-22/problemassesion3/-/blob/master/README.md#grupo-6)

### Grupo 1
Se deberán completar una serie de pedidos para unos clientes que tendrán una disponibilidad de recursos limitada. La herramienta de sincronización que se utilizará en el ejercicio es  [`CyclicBarrier`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/CyclicBarrier.html)  para las diferentes tareas que compondrán el ejercicio. Para ello se deberán realizar las siguientes tareas:  

-   Añadir todas las constantes necesarias en la interface **Constantes** para la resolución del ejercicio.
-   **GestorPedidos**: Esta tarea simulará la preparación de pedidos para unas personas compartidas entre los gestores.
    -   _Atributos_
	    -   Nombre del gestor.
	    -   Lista compartida entre todos los gestores donde estarán disponibles las personas a las que se le prepara el pedido.
	    -   Lista compartida entre todos los gestores donde se almacenará la lista de los pedidos que ha preparado en su ejecución. Cada gestor tendrá su espacio donde almacenar sus pedidos.
	    -   Los elementos de sincronización que sean necesarios.
  
    -   _Ejecución de la tarea_
        -   Esperará hasta que el  **Hilo Principal** de la señal que puede iniciar su trabajo.
        -   Creará un  **Reponedor**  por cada tipo de producto y los ejecutará con los elementos compartidos necesarios.
        -   Hasta que sea interrumpido o finalice su ejecución:
            
            -   Esperará hasta que todos los reponedores le indiquen que ya tiene disponibles los productos.
            -   Se repartirá un producto de cada tipo en cada pedido. Si no hay pedido disponible o no se ha podido asignar el producto se obtendrá una persona de la lista compartida para crear un nuevo pedido donde incluir el producto.
            
            -   Si no hay personas disponibles para crear un nuevo pedido o ya se han preparado un total de pedidos que puede variar entre 8 y 12 finalizará la ejecución de la tarea.
            
            -   Asignar el producto al pedido se simulará con un tiempo de operación que estará comprendido entre 1 y 3 segundos.
            -   Si no tiene productos disponibles avisará a los reponedores para que suministren más productos.
        -   Antes de finalizar se almacenarán los pedidos completados hasta ese momento en la lista compartida con los gestores y avisará al  **Hilo Principal**  que ha completado su tarea. Envía la señal de interrupción a sus proveedores antes de finalizar.
        -   La tarea deberá indicar si ha finalizado su ejecución o si ha sido solicitada la interrupción en su finalización.
-   **Reponedor**  : Será el proceso encargado de entregar productos a sus gestores
    -   _Atributos_
        -   Nombre
        -   Lista de productos compartida con su gestor
        -   Los elementos de sincronización necesarios
    -   _Ejecución_
        -   Hasta que sea interrumpido:
            -   Esperará a que el gestor le indique que necesita más producto y generará una cantidad variable entre 2 y 4 unidades.
            -   Preparar un producto necesita un tiempo de entre 1 y 2 segundos.
            -   Avisará a su gestor que el producto está disponible.
-   **HiloPrincipal**:

	-   Crea tres **GestorPedidos** y les asocia los hilos para su ejecución. Se deben crear las variables compartidas necesarias para estos gestores.
	-   El hilo principal crea 25 personas con recursos variables entre 40 y 60.
	-   Avisará a los gestores que pueden comenzar con su trabajo y esperará a que finalice el primero de ellos.
	-   Solicita la interrupción del resto de gestores y espera hasta que finalicen.
	-   Antes de finalizar presentar la lista de pedidos asociada a cada gestor.

### Grupo 2
Se deberán completar una serie de pedidos para unos clientes que tendrán una disponibilidad de recursos limitada. La herramienta de sincronización que se utilizará en el ejercicio es  [`CyclicBarrier`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/CyclicBarrier.html)  para las diferentes tareas que compondrán el ejercicio. Para ello se deberán realizar las siguientes tareas:  

-   Añadir todas las constantes necesarias en la interface **Constantes** para la resolución del ejercicio.
-   **GestorProcesos**: Esta tarea simulará una gestión básica de procesos atendiendo a su uso de memoria.
    -   _Atributos_
        -   Identificador del gestor.
        -   Lista de procesos que comparte con sus generadores de procesos.
        -   Lista donde almacenar el resultado de la ejecución del gestor. Cada gestor tendrá su espacio en esta lista.
        -   Lista donde almacenar los procesos no asignados por el gestor. Cada gestor tendrá su espacio en esta lista.
        -   Los elementos de sincronización necesarios
    -   _Ejecución_
        -   Creará un **RegistroMemoria** con un número de páginas aleatorio entre 50 y 70.
        -   Repetir hasta que finalice o se solicite su interrupción:
            -   Esperará hasta que uno de sus generadores de procesos le informe que tiene procesos disponibles.
            -   Obtener un proceso de la lista compartida.
            -   Asignarlo al registro de memoria.
            -   Se simulará un tiempo de ejecución para este procedimiento que estará comprendido entre 2 y 4 segundos.
            -   Si no se consigue un proceso de la lista compartida avisa a sus generadores que deben suministrar más.
            -   Si el proceso no se ha incluido en el registro se añadirá a la lista de no asignados.
            -   Finalizará cuando haya asignado un número de procesos entre 15 y 20 a su registro de memoria.
        -   Antes de finalizar deberá almacenar en la lista de resultados el registro de memoria que ha creado el gestor y la lista de procesos no asignados.
        -   La tarea deberá indicar si ha finalizado su ejecución o si ha sido solicitada la interrupción en su finalización.
-   **GeneradorProcesos** : será el encargado de ir creando una serie de procesos para los gestores que se lo solicitan
    -   _Atributos_
        -   Nombre
        -   Lista de procesos compartida con su gestor
        -   Los elementos de sincronización necesarios.
    -   _Ejecución_
        -   Hasta que no sea interrumpido:
            -   Genera un número de procesos entre 3 y 5 de tipos variables.
            -   La generación de un proceso requiere un tiempo entre 1 y 2 segundos.
            -   Indicar al gestor que tiene nuevos procesos disponibles.
            -   Espera a que el gestor le indique que necesita nuevos procesos.
-   **Hilo Principal**:
    -   Se crearán 4 gestores y las variables compartidas necesarias. Se asignarán los hilos necesarios y se ejecutarán.
    -   Se crean entre 2 y 4 generadores de procesos para cada uno de los gestores. Se asignarán los hilos necesarios y se ejecutarán.
    -   Esperará hasta que finalicen la mitad de los gestores y solicitará la interrupción de los gestores y generadores esperando a la finalización de todos ellos.
    -   Antes de finalizar se presentará el resultado de la ejecución de cada uno de los gestores.

### Grupo 3
Se deberán completar una serie de pedidos para unos clientes que tendrán una disponibilidad de recursos limitada. La herramienta de sincronización que se utilizará en el ejercicio es  [Phaser](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/Phaser.html) para las diferentes tareas que compondrán el ejercicio. Para ello se deberán realizar las siguientes tareas:  

-   Añadir todas las constantes necesarias en la interface **Constantes** para la resolución del ejercicio.
-   **Proveedor**: Será una tarea que simula el trabajo de un operador que estará completando una serie de ordenadores según una lista de componentes.
    -   _Atributos_
        -   Nombre del proveedor.
        -   Lista compartida de componentes entre sus fabricantes.
        -   Lista de resultados donde se almacenará, para cada proveedor, la lista de ordenadores que ha realizado durante su ejecución.
        -   Elementos de sincronización necesarios.
    -   _Ejecución_
        -   Creará entre 6 y 10 ordenadores.
        -   Mientras no complete al menos el 80% de los ordenadores o sea interrumpido:
            
            -   Creará un fabricante por cada uno de los tipos de componentes que aún necesite para completar los ordenadores. Los ejecutará con los elementos compartidos necesarios.
            -   Esperará a que todos los fabricantes avisen que han completado su ejecución.
            -   Asignará componentes a los ordenadores hasta completar la lista de componentes.
            
            -   Asignar el componente a un ordenador se simulará con un tiempo de operación que estará comprendido entre 1 y 3 segundos.
        -   Actualiza la lista de resultados con los ordenadores que ha creado.
        -   Si es interrumpido deberá finalizar los fabricantes activo y esperar a su finalización antes de dar por concluida su ejecución.
        -   La tarea deberá indicar si ha finalizado su ejecución o si ha sido solicitada la interrupción en su finalización.  
            
-   **Fabricante**  : simula la construcción de componentes para un ordenador
    -   _Atributos_
        -   Nombre.
        -   Tipo componente.
        -   Lista de componentes compartida con su proveedor.
        -   Elementos de sincronización necesarios.
    -   _Ejecución_
        -   Crea un número de componentes, según el tipo asignado, de entre 2 y 4 y lo añade a la lista compartida.
        -   Cada uno de los componentes requiere un tiempo de creación entre 1 y 3 segundos.
        -   Avisa, antes de finalizar, a su proveedor que ha completado los componentes.
        -   También debe implementarse la interrupción de la tarea.
-   **HiloPrincipal**:
    -   Crear entre 3 y 5 **Proveedor** y les asocia los hilos para su ejecución. Se deben crear las variables compartidas necesarias para estos proveedores.
    -   Esperará a que finalice el 30% de los proveedores.
    -   Interrumpirá el resto de proveedores.
    -   Antes de finalizar se debe presentar la lista de los ordenadores que cada uno de los proveedores ha realizado durante su ejecución.

### Grupo 4
El ejercicio consiste en la preparación recursos de aprendizaje por diferentes universidades. Cada plataforma tendrá asignada una universidad, y esperará a que esta termine de generar recursos para empezar a organizarlos. Las plataformas organizarán recursos en planes hasta que un plan de estudios este completo (tiene un `BLOG`, un `FORO` y al menos `CREDITOS_ASIGNATURA_PLAN_COMPLETO` créditos en `ASIGNATURAS`), momento en el cuál dará por finalizada la fase y se sincronizará con el resto de plataformas, cuando todas completen la fase empezará de nuevo a organizar recursos.
Al final de cada fase los repartidores se sincronizarán para dar comienzo a la siguiente usando la herramienta [`Phaser`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/Phaser.html). Además, las universidadas reducirán un [`CountDownLatch`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/CountDownLatch.html) por cada recurso, y cuando llegue a cero empezará la plataforma a trabajar. Para la solución **solo se pueden usar las herramientas de sincronización anteriores** y los siguientes elementos ya programados:

 - `Utils`: Clase con las constantes y tipos enumerados necesarios para la solución del ejercicio. Hay que utilizar los elementos de esta clase de forma obligatoria para la resolución.
 - `Recurso`: Clase que representa a un recurso de aprendizaje de tipo `TipoRecurso`.
 - `PlanEstudios`: Representa a un plan de estudios. Como máximo puede tener un recurso de blog y foro, no puede tener mas de un foro, ni mas de un blog, pero si un foro y un blog a la vez. Y la suma de créditos de las asignaturas no puede ser mayor a `CREDITOS_ASIGNATURA_MAXIMOS_POR_PLAN`.
	- **Nota**: al plan de estudios se le ha añadido la función isCompleta() que indica si el plan ha sido completado.
	
El ejercicio consiste en completar la implementación de las siguientes clases:
- `Universidad`: Tiene un identificador, una lista de recursos compartida con una plataforma y los elementos de sincronización necesarios.
	 - Deberá crear `RECURSOS_A_GENERAR` recursos, para cada uno:
		 - Al empezar debe esperar un tiempo entre `TIEMPO_CREACION_RECURSOS_MIN` y `TIEMPO_CREACION_RECURSOS_MAX`.
		 - Creará un recurso de un tipo aleatorio con entre `CREDITOS_MINIMOS_ASIGNATURA` y `CREDITOS_MAXIMOS_ASIGNATURA` créditos y lo insertará en el array. El identificador del recurso será único para los creados en la misma universidad.
		 - Llamará a la función de sincronización para confirmar que ha terminado un recurso.
	 - Aunque el hilo sea interrumpido, debe seguir generarando todos los recursos.
	 - Al finalizar imprimirá los datos de la universidad y los recursos generados.
- `Plataforma`: Tiene un identificador, una serie de `PlanEstudios` para completar, un array de recursos compartido con una universidad, un array para los recursos descartados y los elementos de sincronización necesarios.
	- Antes de empezar deberá sincronizarse con la universidad, esperando a que esta termine de generar los recursos.
	 - Para cada recurso:
		 - Localizara el primer plan en el que se pueda insertar y lo insertará.
		 - Esperará un tiempo entre `TIEMPO_ASIGNACION_RECURSOS_MIN` y `TIEMPO_ASIGNACION_RECURSOS_MAX`.
		 - Si el plan en el que se ha insertado el recurso se ha completado, finalizará la fase sincronizandose con el resto de las plataformas.
	 - Cuando finalice el total de recursos o se interrumpa deberá darse de baja del elemento de sincronización con el resto de plataformas.
	 - Se tiene que programar un procedimiento de interrupción.
	 - Debe presentar la lista de los planes que tiene asignados y si ha sido interrumpido debe de mostrar en qué etapa.
- `Hilo principal`: Realizará las siguientes tareas:
	 - Creará las herramientas de sincronización.
	 - Creará un número de `Plataformas` y `Universidades` igual a `REPARTIDORES_A_GENERAR`.
		 - Cada par de repartidor y universidad tendrá asignados un arrayList de recursos y la herramienta de sincronización común.
		 - Para cada repartidor se generará un array de `PLANESTUDIOS_A_GENERAR` planes de estudios.
		 - Todas las plataformas tendrán un array de recursos descartados en común.
		 - Se le asociará un hilo a cada uno para su ejecución.
	 - Se ejecutarán todos los hilos.
	 - Se esperará un `TIEMPO_ESPERA_HILO_PRINCIPAL`, en segundos.
	 - A la finalización de la espera se solicitará la interrupción del trabajo de las plataformas.
	 - Se imprimirán los recursos descartados.
	 - El hilo principal no esperará la finalización de las universidades.

### Grupo 5
El ejercicio consiste en la organización de una serie de reservas de alquiler de coches. Los depósitos preparán coches que los gestores asignarán a las reservas. Para mejorar la eficiencia en el transporte de los coches, la preparación y asignacion se hará en diferentes fases, los depósitos preparan un coche cada uno, y se esperan a que terminen todos para que los gestores los asignen, una vez terminados los gestores, los depósitos comenzaran de nuevo el ciclo. 
Para gestionar esas fases se usarán dos [`CyclicBarrier`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/CyclicBarrier.html). Un `CyclicBarrier` donde esperarán los gestores y los depósitos que vayan terminando de preparar coches, y otro en el que los depósitos esperarán a que los gestores vayan terminando para empezar de nuevo. **Como en cada barrera se tiene que esperar todos los gestores y depósitos, es necesario fijar el número de participantes en `GESTORES_A_GENERAR` \* 2. Para la solución solo se pueden usar las herramientas de sincronización anteriores** y los siguientes elementos ya programados:

- `Utils`: Clase con las constantes y tipos enumerados necesarios para la solución del ejercicio. Hay que utilizar los elementos de esta clase de forma obligatoria para la resolución.
- `Coche`: Identificará a un coche disponible por medio de un número de identificación. También tendrá asociado un TipoReserva que identifica la gama del coche.
- `Reserva`: Identificará a una reserva de alquiler por medio de un número de identificación y contará con una propiedad que indique la gama de coche reservada y cuantos coches son necesarios. Solo se le podrán asignar coches que coincidan con la gama elegida y sin sobrepasar los coches solicitados.

El ejercicio consiste en completar la implementación de las siguientes clases:
- `Depósito`: Este objeto tendrá un identificador, una lista de coche compartida con un gestor y los elementos de sincronización necesarios, dos `CyclicBarrier`.
	 - Repetirá los ciclos hasta que sea interrumpido. En cada ciclo:
		 - Esperará un tiempo entre `TIEMPO_ESPERA_PREPARACION_MIN` y `TIEMPO_ESPERA_PREPARACION_MAX`.
		 - Crear un coche con una gama aleatoria y asignarlo al array. 
		 - Sincronizarse con los depósitos que ya han preparado el coche y con los gestores que están esperando.
		 - Sincronizarse con los depósitos que esperan y con los gestores que están asignando los coches.
	 - Cuando sea interrumpido deberá indicar en que fase se encontraba (en la preparación, en la espera de los depósitos o en la espera de los gestores) y los coches que ha generado.
- `Gestor`: Tiene un identificador, una lista de coches compartida con un gestor, una lista de reservas para ir rellenando, una lista de coches descartados y los elementos de sincronización necesarios, dos `CyclicBarrier`.
	 - Se creará un bucle que solo se puede interrumpir externamente y en cada un de los ciclos:
		 - Cogerá el último coche de la lista, que se acaba de preparar, y lo insertará en la primera reserva que lo admita.
		 - Esperará un tiempo entre `TIEMPO_ESPERA_ASIGNACION_MIN` y `TIEMPO_ESPERA_ASIGNACION_MAX`.
		 - Sincronizarse con los depósitos que ya han preparado el coche y con los gestores que están esperando.
		 - Sincronizarse con los depósitos que esperan y con los gestores que están asignando los coches.
	 - Cuando sea interrumpido deberá indicar en que fase se encontraba (en la espera de los depósitos, en la asignación de los coches o en la espera de los gestores) y los coches que ha asignado.
- `Hilo principal`: Realizará las siguientes tareas:
	 - Creará las herramientas de sincronización.
	 - Creará un número de `Gestor` y `Depósito` igual a `GESTORES_A_GENERAR`.
		 - Cada par de gestor y depósito tendrá asignados un arrayList de jugadores común.
		 - Para cada gestor se generará un array de `RESERVAS_A_GENERAR` reservas.
		 - Todos los gestores tendrán un array de coches descartados en común.
		 - Se le asociará un hilo a cada uno para su ejecución.
	 - Se ejecutarán todos los hilos.
	 - Se esperará un `TIEMPO_ESPERA_HILO_PRINCIPAL`, en segundos.
	 - A la finalización de la espera se solicitará la interrupción del trabajo de los gestores y depósitos.
	 - Se imprimirán los coches descartados.

### Grupo 6
El ejercicio consiste en la construcción de diferentes equipos para poder participar en la competción. Cada organizador tendrá asignado una entidad encargada del registro, y esperará a que la entidad encargada del registro termine de generar jugadores para empezar a organizarlos. Los organizadores asignarán jugadores en equipos hasta que un equipo este completo, momento en el cuál dará por finalizada la fase y se sincronizará con el resto de organizadores, cuando todas completen la fase empezará de nuevo a asginar jugadores.
Al final de cada fase los organizadores se sincronizarán para dar comienzo a la siguiente usando la herramienta [`Phaser`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/Phaser.html). Además, los registradores reducirán un [`CountDownLatch`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/CountDownLatch.html) por cada jugador, y cuando llegue a cero empezará el organizador a trabajar. Para la solución **solo se pueden usar las herramientas de sincronización anteriores** y los siguientes elementos ya programados:

- `Utils`: Clase con las constantes y tipos enumerados necesarios para la solución del ejercicio. Hay que utilizar los elementos de esta clase de forma obligatoria para la resolución.
- `Jugador`: Identificará a un jugador por medio de un número de identificación. También tendrá asociado un Rol para su estilo de juego.
- `Equipo`: Identificará a un equipo por medio de un número de identificación. Como máximo puede tener dos jugadores con el mismo rol y como mínimo dispondrá de un atacante. Y se considerará equipo completo si tiene cuatro jugadores.

El ejercicio consiste en completar la implementación de las siguientes clases:
- `RegistroJugadores`: Tiene un identificador, una lista de jugadores compartida con un organizador y los elementos de sincronización necesarios.
	 - Deberá crear `JUGADORES_A_GENERAR` jugadores, para cada uno:
		 - Al empezar debe esperar un tiempo entre `TIEMPO_CREACION_JUGADORES_MIN` y `TIEMPO_CREACION_JUGADORES_MAX`.
		 - Creará un jugador de un tipo aleatorio y lo insertará en el array. El identificador del jugador será único para los creados en el mismo registrador.
		 - Llamará a la función de sincronización para confirmar que ha terminado un jugador.
	 - Aunque el hilo sea interrumpido, debe seguir registrando jugadores.
	 - Al finalizar imprimirá los datos del registrador y los jugadores generados.
- `Organizador`: Tiene un identificador, una serie de `Equipos` para completar, un array de jugadores compartido con un registrador, un array para los jugadores descartados y los elementos de sincronización necesarios.
	- Antes de empezar deberá sincronizarse con el registrador, esperando a que esta termine de generar los jugadores.
	 - Para cada jugador:
		 - Localizara el primer equipo en el que se pueda insertar y lo insertará.
		 - Esperará un tiempo entre `TIEMPO_ASIGNACION_JUGADORES_MIN` y `TIEMPO_ASIGNACION_JUGADORES_MAX`.
		 - Si el equipo en el que se ha insertado el jugador se ha completado, finalizará la fase sincronizandose con el resto de los organizadores.
	 - Cuando finalice el total de jugadores o se interrumpa deberá darse de baja del elemento de sincronización con el resto de organizadores.
	 - Se tiene que programar un procedimiento de interrupción.
	 - Debe presentar la lista de los equipos que tiene asignados y si ha sido interrumpido debe de mostrar en qué etapa.
- `Hilo principal`: Realizará las siguientes tareas:
	 - Creará las herramientas de sincronización.
	 - Creará un número de `Organizadores` y `RegistroJugadores` igual a `EQUIPOS_A_GENERAR`.
		 - Cada par de organizador y registrador tendrá asignados un arrayList de jugadores y la herramienta de sincronización común.
		 - Para cada organizador se generará un array de `EQUIPOS_A_GENERAR` reservas.
		 - Todos los organizadores tendrán un array de jugadores descartados en común.
		 - Se le asociará un hilo a cada uno para su ejecución.
	 - Se ejecutarán todos los hilos.
	 - Se esperará un `TIEMPO_ESPERA_HILO_PRINCIPAL`, en segundos.
	 - A la finalización de la espera se solicitará la interrupción del trabajo de los organizadores.
	 - Se imprimirán los jugadores descartados.
	 - El hilo principal no esperará la finalización de los registradores.
