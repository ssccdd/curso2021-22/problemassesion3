/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2122.problemassesion3.grupo4;

import static es.uja.ssccdd.curso2122.problemassesion3.grupo4.Utils.*;
import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Phaser;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Sesion3 {

    public static void main(String[] args) {
        // Variables aplicación
        int idPlanes = 0;

        ArrayList<Thread> hilosPlataformas = new ArrayList<>();
        ArrayList<Thread> hilosUniversidades = new ArrayList<>();
        ArrayList<Recurso> recursosPerdidos = new ArrayList<>();
        Phaser sincOrganizadores = new Phaser(PLATAFORMAS_A_GENERAR);

        // Ejecución del hilo principal
        System.out.println("HILO-Principal Ha iniciado la ejecución");

        for (int i = 0; i < PLATAFORMAS_A_GENERAR; i++) {

            // Inicializamos los planes
            ArrayList<PlanEstudios> listaPlanes = new ArrayList<>();
            for (int j = 0; j < PLANESTUDIOS_A_GENERAR; j++) {
                listaPlanes.add(new PlanEstudios(idPlanes++));
            }

            // Inicializamos los recursos
            ArrayList<Recurso> listaRecursos = new ArrayList<>();

            // Inicializamos los registros 
            CountDownLatch sincRegistro = new CountDownLatch(RECURSOS_A_GENERAR);
            Universidad universidad = new Universidad(i, listaRecursos, sincRegistro);
            Thread threadRegistro = new Thread(universidad, "Registro " + i);
            threadRegistro.start();
            hilosUniversidades.add(threadRegistro);

            // Inicializamos las plataformas
            PlataformaAprendizaje plataforma = new PlataformaAprendizaje(i, listaPlanes, listaRecursos, recursosPerdidos, sincRegistro, sincOrganizadores);
            Thread thread = new Thread(plataforma, "Plataforma " + i);
            thread.start();
            hilosPlataformas.add(thread);

        }

        System.out.println("HILO-Principal Espera a la finalización de las plataformas");

        try {
            TimeUnit.SECONDS.sleep(TIEMPO_ESPERA_HILO_PRINCIPAL);
        } catch (InterruptedException ex) {
            Logger.getLogger(Sesion3.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.println("HILO-Principal Solicita la finalización de las plataformas");

        for (int i = 0; i < PLATAFORMAS_A_GENERAR; i++) {
            hilosPlataformas.get(i).interrupt();
        }

        System.out.println("HILO-Principal Espera a las plataformas");

        for (int i = 0; i < PLATAFORMAS_A_GENERAR; i++) {
            try {
                hilosPlataformas.get(i).join();
            } catch (InterruptedException ex) {
                Logger.getLogger(Sesion3.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        System.out.println("\nLos siguientes " + recursosPerdidos.size() + " recursos no han podido ser insertados:");
        for (Recurso p : recursosPerdidos) {
            System.out.println("\t" + p);
        }

        System.out.println("HILO-Principal Ha finalizado la ejecución");
    }

}
