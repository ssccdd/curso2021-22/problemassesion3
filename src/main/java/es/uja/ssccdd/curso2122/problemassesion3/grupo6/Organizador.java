/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2122.problemassesion3.grupo6;

import static es.uja.ssccdd.curso2122.problemassesion3.grupo6.Utils.*;
import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Phaser;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author UJA
 */
public class Organizador implements Runnable {

    private final int iD;
    private final ArrayList<Equipo> equipos;
    private final ArrayList<Jugador> jugadoresPendientes;
    private final ArrayList<Jugador> jugadoresNoAsignados;
    private final CountDownLatch sincCreacionJugadores;
    private final Phaser sincEntreOrganizadores;
    private boolean interrumpido;
    private int jugadoresProcesados;
    private int equiposCompletados;

    public Organizador(int iD, ArrayList<Equipo> equipos, ArrayList<Jugador> jugadoresPendientes, ArrayList<Jugador> jugadoresNoAsignados, CountDownLatch sincCreacionJugadores, Phaser sincEntreOrganizadores) {
        this.iD = iD;
        this.equipos = equipos;
        this.jugadoresPendientes = jugadoresPendientes;
        this.jugadoresNoAsignados = jugadoresNoAsignados;
        this.sincCreacionJugadores = sincCreacionJugadores;
        this.sincEntreOrganizadores = sincEntreOrganizadores;
        this.interrumpido = false;
        this.jugadoresProcesados = 0;
        this.equiposCompletados = 0;
    }

    @Override
    public void run() {

        System.out.println(Thread.currentThread().getName() + " ha comenzado, esperando a que termine el registro.");

        try {
            sincCreacionJugadores.await();
            System.out.println(Thread.currentThread().getName() + " terminado el registro, empezando organización.");

            for (int i = 0; i < jugadoresPendientes.size() && !interrumpido; i++) {

                //Se prueba a insertar un jugador en los equipos disponibles
                int equipoInsertado = organizarJugador(i);

                //En cualquier caso, se marca como procesado
                jugadoresProcesados++;

                try {
                    TimeUnit.MILLISECONDS.sleep(Utils.random.nextInt(TIEMPO_ASIGNACION_JUGADORES_MAX - TIEMPO_ASIGNACION_JUGADORES_MIN) + TIEMPO_ASIGNACION_JUGADORES_MIN);
                } catch (InterruptedException ex) {
                    //Si nos interrupten en la espera del último jugador, no lo contamos.
                    if (jugadoresPendientes.size() != jugadoresProcesados) {
                        interrumpido = true;
                    }
                }
                try {
                    if (equipoInsertado >= 0) {//Ha sido insertado
                        if (equipos.get(equipoInsertado).isCompleto()) {//He completado un equipo
                            equiposCompletados++;
                                System.out.println(Thread.currentThread().getName() + " ha completado al equipo " + equiposCompletados + " y esperará al resto de los organizadores");
                                sincEntreOrganizadores.awaitAdvanceInterruptibly(sincEntreOrganizadores.arrive());
                        }
                    }
                } catch (InterruptedException ex) {
                    interrumpido = true;
                }

            }
        } catch (InterruptedException ex) {
            System.out.println("WARNING: El organizador " + iD + " ha sido interrumpido mientras esperaba a los registros.");
        }
        
        
        sincEntreOrganizadores.arriveAndDeregister();// El hilo ha terminado
        

        System.out.println(this.toString());

    }

    /**
     * Intenta introducir un juagdor en algun equipo
     *
     * @param indiceJugador indice del jugador a insertar
     * @return true si ha podido ser insertado en algun equipo
     */
    private int organizarJugador(int indiceJugador) {

        int insertado = -1;
        for (int j = 0; j < equipos.size() && insertado < 0; j++) {
            if (equipos.get(j).addJugador(jugadoresPendientes.get(indiceJugador))) {
                insertado = j;
            }
        }

        //Si no se ha podido insertar se guarda la incidencia
        if (insertado < 0) {
            jugadoresNoAsignados.add(jugadoresPendientes.get(indiceJugador));
        }

        return insertado;

    }

    /**
     * Calcula el porcentaje de trabajo realizado
     *
     * @return procentaje de trabajo realizado
     */
    private float porcentajeCompletado() {
        return 1.0f * jugadoresProcesados / jugadoresPendientes.size();
    }

    /**
     * Devuelve una cadena con la información de la clase
     *
     * @return Cadena de texto con la información
     */
    @Override
    public String toString() {
        StringBuilder mensaje = new StringBuilder();
        mensaje.append(Thread.currentThread().getName()).append(" ");

        if (interrumpido) {
            mensaje.append("\"INTERRUMPIDO\" en la fase ").append(equiposCompletados);
        }

        mensaje.append(", ").append(jugadoresPendientes.size()).append(" jugadores disponibles, de los cuales se han procesado el ").append(porcentajeCompletado() * 100).append("%.");
        mensaje.append("\n Equipos disponibles ").append(equipos.size()).append(", equipos completados ").append(equiposCompletados).append(".");

        for (Equipo equipo : equipos) {
            mensaje.append("\n\t").append(equipo.toString());
        }

        if (jugadoresProcesados < jugadoresPendientes.size()) {
            mensaje.append("\n\tSe han quedado fuera ").append(jugadoresPendientes.size() - jugadoresProcesados).append(" jugadores por falta de tiempo.");
        }

        return mensaje.toString();
    }

}
