/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2122.problemassesion3.grupo4;

import static es.uja.ssccdd.curso2122.problemassesion3.grupo4.Utils.*;
import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author UJA
 */
public class Universidad implements Runnable {

    private final int id;
    private final ArrayList<Recurso> recursosCreados;
    private final CountDownLatch sincCreacionRecursos;

    public Universidad(int id, ArrayList<Recurso> recursosCreados, CountDownLatch sincCreacionJugadores) {
        this.id = id;
        this.recursosCreados = recursosCreados;
        this.sincCreacionRecursos = sincCreacionJugadores;
    }

    @Override
    public void run() {

        int identificador = 0;
        System.out.println(Thread.currentThread().getName() + " ha comenzado.");
        
        for (int i = 0; i < RECURSOS_A_GENERAR; i++) {

            try {
                int tiempoEspera = random.nextInt(TIEMPO_CREACION_RECURSOS_MAX - TIEMPO_CREACION_RECURSOS_MIN) + TIEMPO_CREACION_RECURSOS_MIN;
                TimeUnit.MILLISECONDS.sleep(tiempoEspera);

                int aleatorioRecurso = random.nextInt(VALOR_GENERACION);
                int aleatorioCreditos = random.nextInt((CREDITOS_MAXIMOS_ASIGNATURA + 1) - CREDITOS_MINIMOS_ASIGNATURA) + CREDITOS_MINIMOS_ASIGNATURA;
                recursosCreados.add(new Recurso(identificador++, TipoRecurso.getRecurso(aleatorioRecurso), aleatorioCreditos));

            } catch (InterruptedException ex) {
                //Este hilo no tiene que ser interrumpido por lo que no hace nada si es interrumpido.
            } finally {
                sincCreacionRecursos.countDown();
            }
        }

    }

    /**
     * Devuelve una cadena con la información de la clase
     *
     * @return Cadena de texto con la información
     */
    @Override
    public String toString() {
        StringBuilder mensaje = new StringBuilder();
        mensaje.append(Thread.currentThread().getName()).append(" ");

        mensaje.append("\n Recursos generados:  ").append(recursosCreados.size()).append(": ");

        for (Recurso recurso : recursosCreados) {
            mensaje.append("\n\t").append(recurso.toString());
        }

        return mensaje.toString();
    }

}
