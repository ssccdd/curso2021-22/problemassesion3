/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2122.problemassesion3.grupo1;

import static es.uja.ssccdd.curso2122.problemassesion3.grupo1.Constantes.MIN_PRODUCTOS;
import es.uja.ssccdd.curso2122.problemassesion3.grupo1.Constantes.TipoProducto;
import static es.uja.ssccdd.curso2122.problemassesion3.grupo1.Constantes.VARIACION_PRODUCTOS;
import static es.uja.ssccdd.curso2122.problemassesion3.grupo1.Constantes.aleatorio;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.TimeUnit;
import static es.uja.ssccdd.curso2122.problemassesion3.grupo1.Constantes.MIN_TIEMPO_REPONEDOR;
import static es.uja.ssccdd.curso2122.problemassesion3.grupo1.Constantes.VARIACION_TIEMPO_REPONEDOR;

/**
 *
 * @author pedroj
 */
public class Reponedor implements Runnable {
    private final String iD;
    private final int[] listaProductos;
    private final TipoProducto tipoProducto;
    private final CyclicBarrier iniciaReposicion;
    private final CyclicBarrier finReposicion;

    public Reponedor(String iD, int[] listaProductos, TipoProducto tipoProducto, 
                     CyclicBarrier iniciaReposicion, CyclicBarrier finReposicion) {
        this.iD = iD;
        this.listaProductos = listaProductos;
        this.tipoProducto = tipoProducto;
        this.iniciaReposicion = iniciaReposicion;
        this.finReposicion = finReposicion;
    }

    @Override
    public void run() {
        System.out.println("El HILO(" + iD + ") comienza su ejecución");
        
        try {
            while(true) {
                reposicion();
            }
        } catch(InterruptedException | BrokenBarrierException ex) {
            System.out.println("El HILO(" + iD + ") ha FINALIZADO");      
        } 
    }

    public String getiD() {
        return iD;
    }
    
    private void reposicion() throws InterruptedException, BrokenBarrierException {
        int elementos;
        
        // Espera a que el gestos avise que necesita nuevo producto
        this.iniciaReposicion.await();
        
        // Repone los productos solicitados
        elementos = MIN_PRODUCTOS + aleatorio.nextInt(VARIACION_PRODUCTOS);
        for(int i = 0; i < elementos; i++) {
            int produccion = MIN_TIEMPO_REPONEDOR + aleatorio.nextInt(VARIACION_TIEMPO_REPONEDOR);
            TimeUnit.SECONDS.sleep(produccion);
        }
        
        // Intorma que ha finalizado la reposición
        System.out.println("El HILO(" + iD + ") ha repuesto " + elementos + " " + tipoProducto);
        this.listaProductos[this.tipoProducto.ordinal()] = elementos;
        this.finReposicion.await();
    }  
}
