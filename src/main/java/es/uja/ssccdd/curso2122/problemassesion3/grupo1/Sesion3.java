/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2122.problemassesion3.grupo1;

import static es.uja.ssccdd.curso2122.problemassesion3.grupo1.Constantes.FINALIZA_EL_PRIMERO;
import static es.uja.ssccdd.curso2122.problemassesion3.grupo1.Constantes.INCREMENTO_RECURSOS;
import static es.uja.ssccdd.curso2122.problemassesion3.grupo1.Constantes.MIN_RECURSOS;
import static es.uja.ssccdd.curso2122.problemassesion3.grupo1.Constantes.NUM_GESTORES;
import static es.uja.ssccdd.curso2122.problemassesion3.grupo1.Constantes.NUM_PERSONAS;
import static es.uja.ssccdd.curso2122.problemassesion3.grupo1.Constantes.aleatorio;
import java.util.ArrayList;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

/**
 *
 * @author UJA
 */
public class Sesion3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException, BrokenBarrierException {
        Thread[] listaHilos;
        GestorPedidos gestor;
        ArrayList<Persona> listaPersonas;
        ArrayList<Pedido>[] listaResultados;
        Persona persona;
        CyclicBarrier finGestor;
        CyclicBarrier inicioGestores;
        
        // Ejecución del hilo principal
        System.out.println("Ha iniciado la ejecución el Hilo(PRINCIPAL)");
        
        // Inicialización de variables
        listaPersonas = new ArrayList();
        listaResultados = new ArrayList[NUM_GESTORES];
        listaHilos = new Thread[NUM_GESTORES];
        inicioGestores = new CyclicBarrier(NUM_GESTORES + 1);
        finGestor = new CyclicBarrier(FINALIZA_EL_PRIMERO);
        for(int i = 0; i < NUM_GESTORES; i++) {
            listaResultados[i] = new ArrayList();
            gestor = new GestorPedidos("Gestor-" + i, listaPersonas, listaResultados[i], 
                                       inicioGestores, finGestor);
            listaHilos[i] = new Thread(gestor);
        }
        
        // Cuerpo de ejecución del hilo principal
        for(int i = 0; i < NUM_PERSONAS; i++) {
            persona = new Persona("Persona-" + i, MIN_RECURSOS + 
                                                  aleatorio.nextInt(INCREMENTO_RECURSOS));
            listaPersonas.add(persona);
        }
        
        // Se ejecutan los gestores
        for(Thread hilo : listaHilos)
            hilo.start();
        
        System.out.println("Hilo(PRINCIPAL) se inician los GESTORES");
        inicioGestores.await();

        System.out.println("Hilo(PRINCIPAL) espera a que termine un GESTOR");
        finGestor.await();
        
        System.out.println("Hilo(PRINCIPAL) solicita la INTERRUPCION de los gestores y espera a su finalización");
        for(Thread hilo : listaHilos)
            hilo.interrupt();
        
        for(Thread hilo : listaHilos)
            hilo.join();
        
        // Presentar resultados
        for(int i = 0; i < NUM_GESTORES; i++) {
            System.out.println("_______ Resultados Gestor " + i + " _____________");
            for(Pedido pedido : listaResultados[i])
                System.out.println(pedido);
            System.out.println("_________________________________________________");
        }
        
        // Finalización
        System.out.println("Ha finalizado la ejecución el Hilo(PRINCIPAL)");
    }
    
}
