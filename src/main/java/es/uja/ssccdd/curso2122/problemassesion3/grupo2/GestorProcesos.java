/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2122.problemassesion3.grupo2;

import static es.uja.ssccdd.curso2122.problemassesion3.grupo2.Constantes.MIN_PROCESOS_PARA_ASIGNAR;
import static es.uja.ssccdd.curso2122.problemassesion3.grupo2.Constantes.MIN_TIEMPO_PROCESADO;
import static es.uja.ssccdd.curso2122.problemassesion3.grupo2.Constantes.NINGUNO;
import static es.uja.ssccdd.curso2122.problemassesion3.grupo2.Constantes.PRIMERO;
import static es.uja.ssccdd.curso2122.problemassesion3.grupo2.Constantes.VARIACION_ASIGNACION_PROCESOS;
import static es.uja.ssccdd.curso2122.problemassesion3.grupo2.Constantes.VARIACION_TIEMPO_PROCESADO;
import static es.uja.ssccdd.curso2122.problemassesion3.grupo2.Constantes.aleatorio;
import java.util.ArrayList;
import java.util.Optional;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pedroj
 */
public class GestorProcesos implements Runnable {
    private final String iD;
    private final ArrayList<Proceso> listaProcesos;
    private final RegistroMemoria resultadoEjecucion;
    private final ArrayList<Proceso> listaNoAsignados;
    private final CyclicBarrier inicioGeneracion;
    private final CyclicBarrier finGeneracion;
    private final CyclicBarrier finEjecucion;
    private int procesosParaAsignar;

    public GestorProcesos(String iD, ArrayList<Proceso> listaProcesos, RegistroMemoria resultadoEjecucion, 
                          ArrayList<Proceso> listaNoAsignados, CyclicBarrier inicioGeneracion, 
                          CyclicBarrier finGeneracion, CyclicBarrier finEjecucion) {
        this.iD = iD;
        this.listaProcesos = listaProcesos;
        this.resultadoEjecucion = resultadoEjecucion;
        this.listaNoAsignados = listaNoAsignados;
        this.inicioGeneracion = inicioGeneracion;
        this.finGeneracion = finGeneracion;
        this.finEjecucion = finEjecucion;
        
        // Procesos para asignar
        this.procesosParaAsignar = MIN_PROCESOS_PARA_ASIGNAR + 
                                   aleatorio.nextInt(VARIACION_ASIGNACION_PROCESOS);
    }

    
    @Override
    public void run() {
        Optional<Proceso> proceso = Optional.empty();
        
        System.out.println("El HILO(" + iD + ") comienza su ejecución para asignar "
                            + procesosParaAsignar + " procesos");
        
        try {
            // Hasta su finalización o su interrupción
            solicitarProcesos();
            
            while( procesosParaAsignar > NINGUNO ) {
                // Esperamos a que un generador finalice
                esperarProcesos();
                
                // Podemos no obtener un proceso
                proceso = obtenerProceso();
                while( proceso.isPresent() && (procesosParaAsignar > NINGUNO) ) {
                    Proceso procesoAsignar = proceso.get();
                    asignarProceso(procesoAsignar);
                    System.out.println("El HILO(" + iD + ") añade el proceso " + proceso +
                                       " a su registro de memoria." +
                                       "\n\tQuedan por asignar " + procesosParaAsignar);
                    proceso = obtenerProceso();
                }
                
                // Solicitamos procesos si no tenemos ninguno disponible
                if( !proceso.isPresent() && (procesosParaAsignar > NINGUNO) ) 
                    solicitarProcesos();
            }
            
            System.out.println("El HILO(" + iD + ") ha finalizado su ejecución");
        } catch(InterruptedException ex) {
            System.out.println("El HILO(" + iD + ") ha sido INTERRUMPIDO");
        } catch(BrokenBarrierException ex) {
            System.out.println("El HILO(" + iD + ") ha ocurrido un ERROR " + ex);
        } finally {
            finalizarEjecucion();
        }
    }

    public String getiD() {
        return iD;
    }
    
    private void finalizarEjecucion() {
        try {
            // Indica al hilo principal que ha finalizado
            this.finEjecucion.await();
        } catch (InterruptedException | BrokenBarrierException ex) {
            // Capturamos la excepción y no hacemos nada porque estamos finalizando
        }
    }
    
    private void esperarProcesos() throws InterruptedException, BrokenBarrierException {
        // Esperar a un generador y reiniciar el punto de sincronización
        this.finGeneracion.await();
        this.finGeneracion.reset();
        System.out.println("El HILO(" + iD + ") ha finalizado un generador");
    }
    
    private void solicitarProcesos() throws InterruptedException, BrokenBarrierException {
        // Indica a los generadores que se necesitan más procesos 
        // y reinicia el punto de sincronización
        System.out.println("El HILO(" + iD + ") solicita nuevos pocesos");
        this.inicioGeneracion.await();
        this.inicioGeneracion.reset();
    }
    
    /**
     * Obtenemos un proceso de la lista compartida con sus generadores
     * Solo se permite la interrupción si no hemos intentado la recuperación
     * de un proceso.
     * @return un objeto opcional que contiene el pedido si está disponible.
     * @throws InterruptedException 
     */
    private Optional<Proceso> obtenerProceso() throws InterruptedException {
        Optional<Proceso> proceso;
        int tiempo;
        
        // Simulamos el tiempo de operación
        tiempo = MIN_TIEMPO_PROCESADO + aleatorio.nextInt(VARIACION_TIEMPO_PROCESADO);
        TimeUnit.SECONDS.sleep(tiempo);
        
        try {
            proceso = Optional.ofNullable(listaProcesos.remove(PRIMERO));
        } catch(IndexOutOfBoundsException ex) {
            // No hay procesos en la lista
            proceso = Optional.empty();
        } 
        
        return proceso;
    }
    
    /** 
     * Asigna un proceso al registro de memoria si es posible o lo añade a su 
     * lista de procesos no asignados.
     * @param procesoAsignar 
     */
    private void asignarProceso(Proceso procesoAsignar) {
        
        if( resultadoEjecucion.addProceso(procesoAsignar) )
            procesosParaAsignar--;
        else
            listaNoAsignados.add(procesoAsignar);
    } 
}
