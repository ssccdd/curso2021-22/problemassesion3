/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2122.problemassesion3.grupo5;

import static es.uja.ssccdd.curso2122.problemassesion3.grupo5.Utils.*;
import java.util.ArrayList;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Sesion3 {

    public static void main(String[] args) {

        // Variables aplicación
        int idReservas = 0;

        ArrayList<Thread> hilosGestores = new ArrayList<>();
        ArrayList<Thread> hilosDepositos = new ArrayList<>();
        ArrayList<Coche> cochesPerdidos = new ArrayList<>();
        CyclicBarrier sincPreparacion = new CyclicBarrier(GESTORES_A_GENERAR * 2);
        CyclicBarrier sincAsignacion = new CyclicBarrier(GESTORES_A_GENERAR * 2);

        // Ejecución del hilo principal
        System.out.println("HILO-Principal Ha iniciado la ejecución");

        for (int i = 0; i < GESTORES_A_GENERAR; i++) {

            // Inicializamos las reservas
            ArrayList<Reserva> listaReservas = new ArrayList<>();
            for (int j = 0; j < RESERVAS_A_GENERAR; j++) {
                int aleatorioTipo = random.nextInt(VALOR_GENERACION);
                int aleatorioNCoches = random.nextInt(MAXIMO_COCHES_POR_RESERVA) + 1;
                listaReservas.add(new Reserva(idReservas++, aleatorioNCoches, TipoReserva.getTipoReserva(aleatorioTipo)));

            }

            // Inicializamos la lista de coches para depositos y gestores
            ArrayList<Coche> listaCoches = new ArrayList<>();
            

            // Inicializamos los depositos y gestores
            Deposito deposito = new Deposito(i, listaCoches, sincAsignacion, sincPreparacion);
            Thread threadDeposito = new Thread(deposito, "Deposito "+ i);
            threadDeposito.start();
            hilosDepositos.add(threadDeposito);
            
            Gestor gestor = new Gestor(i, listaReservas, listaCoches, cochesPerdidos, sincAsignacion, sincPreparacion);
            Thread threadGestor = new Thread(gestor,"Gestor " + i);
            threadGestor.start();
            hilosGestores.add(threadGestor);

        }

        System.out.println("HILO-Principal Espera a la finalización de los gestores y depositos");
        try {
            TimeUnit.SECONDS.sleep(TIEMPO_ESPERA_HILO_PRINCIPAL);
        } catch (InterruptedException ex) {
            Logger.getLogger(Sesion3.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.println("HILO-Principal Solicita la finalización de los gestores y depósitos");
        for (int i = 0; i < GESTORES_A_GENERAR; i++) {
            hilosGestores.get(i).interrupt();
            hilosDepositos.get(i).interrupt();
        }

        System.out.println("HILO-Principal Espera a los gestoresy depósitos");
        for (int i = 0; i < GESTORES_A_GENERAR; i++) {
            try {
                hilosGestores.get(i).join();
                hilosDepositos.get(i).join();
            } catch (InterruptedException ex) {
                Logger.getLogger(Sesion3.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        System.out.println("\nLos siguientes " + cochesPerdidos.size() + " coches no han podido ser insertados:");
        for (Coche coche : cochesPerdidos) {
            System.out.println("\t" + coche);
        }

        System.out.println("HILO-Principal Ha finalizado la ejecución");
    }

}
