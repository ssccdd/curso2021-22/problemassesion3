/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2122.problemassesion3.grupo5;

import java.util.ArrayList;

import static es.uja.ssccdd.curso2122.problemassesion3.grupo5.Utils.TipoReserva.*;
import static es.uja.ssccdd.curso2122.problemassesion3.grupo5.Utils.*;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author UJA
 */
public class Gestor implements Runnable {

    private final int iD;
    private final ArrayList<Reserva> reservas;
    private final ArrayList<Coche> cochesPendientes;
    private final ArrayList<Coche> cochesNoAsignados;
    private final CyclicBarrier sincPreparacion;
    private final CyclicBarrier sincAsignacion;
    private int faseInterrupción;
    private int cochesProcesados;

    public Gestor(int iD, ArrayList<Reserva> reservas, ArrayList<Coche> cochesPendientes, ArrayList<Coche> cochesNoAsignados, CyclicBarrier sincPreparacion, CyclicBarrier sincAsignacion) {
        this.iD = iD;
        this.reservas = reservas;
        this.cochesPendientes = cochesPendientes;
        this.cochesNoAsignados = cochesNoAsignados;
        this.sincPreparacion = sincPreparacion;
        this.sincAsignacion = sincAsignacion;
        this.faseInterrupción = 0;
        this.cochesProcesados = 0;
    }

    @Override
    public void run() {

        System.out.println(Thread.currentThread().getName() + " ha comenzado.");

        try {
            while (true) {

                faseInterrupción = 0;
                System.out.println(Thread.currentThread().getName() + " esperando la preparación de los coches.");
                sincPreparacion.await();

                faseInterrupción = 1;
                System.out.println(Thread.currentThread().getName() + " realizando la asignación del coche.");
                TimeUnit.MILLISECONDS.sleep(Utils.random.nextInt(TIEMPO_ESPERA_ASIGNACION_MAX - TIEMPO_ESPERA_ASIGNACION_MIN) + TIEMPO_ESPERA_ASIGNACION_MIN);
                organizarCoche(cochesPendientes.size() - 1);
                cochesProcesados++;

                faseInterrupción = 2;
                System.out.println(Thread.currentThread().getName() + " esperando la asignación del resto de coche.");
                sincAsignacion.await();
            }
        } catch (Exception ex) {

        }
        System.out.println(this.toString());

    }

    /**
     * Intenta introducir un coche en alguna reserva
     *
     * @param indiceCoche indice del coche a insertar
     * @return true si ha podido ser insertado en alguna reserva
     */
    private boolean organizarCoche(int indiceCoche) {

        Coche coche = cochesPendientes.get(indiceCoche);
        boolean encolado = false;

        for (int i = 0; i < reservas.size() && !encolado; i++) {
            if (reservas.get(i).addCoche(coche)) {
                encolado = true;
            }
        }

        //Si no se ha podido insertar se guarda la incidencia
        if (!encolado) {
            cochesNoAsignados.add(cochesPendientes.get(indiceCoche));
        }

        return encolado;

    }

    /**
     * Calcula el porcentaje de trabajo realizado
     *
     * @return procentaje de trabajo realizado
     */
    private float porcentajeCompletado() {
        return 1.0f * cochesProcesados / cochesPendientes.size();
    }

    /**
     * Devuelve una cadena con la información de la clase
     *
     * @return Cadena de texto con la información
     */
    @Override
    public String toString() {
        StringBuilder mensaje = new StringBuilder();
        mensaje.append("\n\nGestor ").append(iD).append(" ");

        mensaje.append("\"INTERRUMPIDO\" ").append(faseInterrupción==0 ? "esperando coche" :(faseInterrupción==1?"asignando coche" : "esperando la asignación del resto"));


        mensaje.append(" ").append(cochesPendientes.size()).append(" coches disponibles, de los cuales se han procesado el ").append(porcentajeCompletado() * 100).append("%.");

        for (Reserva reserva : reservas) {
            mensaje.append("\n\t").append(reserva.toString());
        }

        if (cochesProcesados < cochesPendientes.size()) {
            mensaje.append("\n\tSe han quedado fuera ").append(cochesPendientes.size() - cochesProcesados).append(" coches por falta de tiempo.");
        }

        return mensaje.toString();
    }

}
