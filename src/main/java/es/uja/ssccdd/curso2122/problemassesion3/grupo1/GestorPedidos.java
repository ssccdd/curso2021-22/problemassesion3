/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2122.problemassesion3.grupo1;

import static es.uja.ssccdd.curso2122.problemassesion3.grupo1.Constantes.ASIGNADO;
import static es.uja.ssccdd.curso2122.problemassesion3.grupo1.Constantes.FIN_EJECUCION;
import static es.uja.ssccdd.curso2122.problemassesion3.grupo1.Constantes.MIN_PEDIDOS;
import static es.uja.ssccdd.curso2122.problemassesion3.grupo1.Constantes.MIN_TIEMPO_PROCESADO;
import static es.uja.ssccdd.curso2122.problemassesion3.grupo1.Constantes.NO_HAY_PRODUCTO;
import static es.uja.ssccdd.curso2122.problemassesion3.grupo1.Constantes.PRIMERO;
import es.uja.ssccdd.curso2122.problemassesion3.grupo1.Constantes.TipoProducto;
import static es.uja.ssccdd.curso2122.problemassesion3.grupo1.Constantes.VARIACION_PEDIDOS;
import static es.uja.ssccdd.curso2122.problemassesion3.grupo1.Constantes.VARIACION_TIEMPO_PROCESADO;
import static es.uja.ssccdd.curso2122.problemassesion3.grupo1.Constantes.aleatorio;
import static es.uja.ssccdd.curso2122.problemassesion3.grupo1.Constantes.productos;
import java.util.ArrayList;
import java.util.Optional;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pedroj
 */
public class GestorPedidos implements Runnable {
    private final String iD;
    private final ArrayList<Persona> listaPersonas;
    private final ArrayList<Pedido> resultadoEjecucion;
    private final int[] listaProductos;
    private final CyclicBarrier inicioEjecucion;
    private final CyclicBarrier finEjecucion;
    private final CyclicBarrier iniciaReposicion;
    private final CyclicBarrier finReposicion;
    private int indicePedido;
    private int ultimoProducto;
    private int numPedidos;

    public GestorPedidos(String iD, ArrayList<Persona> listaPersonas, 
                         ArrayList<Pedido> resultadoEjecucion, CyclicBarrier inicioEjecucion, 
                         CyclicBarrier finEjecucion) {
        this.iD = iD;
        this.listaPersonas = listaPersonas;
        this.resultadoEjecucion = resultadoEjecucion;
        this.inicioEjecucion = inicioEjecucion;
        this.finEjecucion = finEjecucion;
        
        // Lista donde se intercambian los productos con los reponedores
        this.listaProductos = new int[productos.length];
        
        // Elementos de sincronización con los reponedores
        this.iniciaReposicion = new CyclicBarrier(productos.length + 1);
        this.finReposicion = new CyclicBarrier(productos.length + 1);
        
        // Indice del pedido para distribuir los productos en los pedidos
        this.indicePedido = PRIMERO;
        
        // Indice del último producto asignado
        this.ultimoProducto = PRIMERO;
        
        // Pedidos a preparar
        this.numPedidos = MIN_PEDIDOS + aleatorio.nextInt(VARIACION_PEDIDOS); 
    }

    @Override
    public void run() {
        boolean finalTarea;
        Optional<TipoProducto> producto;
        Thread[] hilosReponedores = null;
        
        System.out.println("El HILO(" + iD + ") comienza su ejecución para " + numPedidos +
                           " pedidos");
        finalTarea = !FIN_EJECUCION;
        
        try {
            hilosReponedores = inicioReponedores();
            
            // Hasta su finalización o su interrupción
            do {
                System.out.println("El HILO(" + iD + ") pide reposición");
                reponerProducto();
               
                producto = obtenerProducto();
                while( producto.isPresent() && !finalTarea) {
                    if( !asignarProducto(producto.get()) )
                        finalTarea = nuevoPedido(producto.get());
                    producto = obtenerProducto();
                }
            } while(!finalTarea);
            
            System.out.println("El HILO(" + iD + ") ha finalizado su ejecución");
        } catch(InterruptedException ex) {
            System.out.println("El HILO(" + iD + ") ha sido INTERRUMPIDO");
        } catch (BrokenBarrierException ex) {
            System.out.println("El HILO(" + iD + ") ha ocurrido un ERROR en la ejecucion" +
                                "\n\t " + ex);
        } finally {
            finalizarReponedores(hilosReponedores);
        }
    }

    public String getiD() {
        return iD;
    }
    
    private Thread[] inicioReponedores() throws InterruptedException, BrokenBarrierException {
        // Hasta que no lo indique el hilo principal no se empieza
        inicioEjecucion.await();
        
        Thread[] hilosReponedores = new Thread[productos.length];
        
        for(int i = 0; i < hilosReponedores.length; i++) {
            Reponedor reponedor = new Reponedor("Reponedor-" + iD + "-" + i, listaProductos,
                                                productos[i], iniciaReposicion, finReposicion);
            hilosReponedores[i] = new Thread(reponedor);
            hilosReponedores[i].start();
        }
        
        return hilosReponedores;
    }
    
    private void finalizarReponedores(Thread[] hilosReponedores) {
        for(Thread hilo : hilosReponedores)
            hilo.interrupt();
        
        for(Thread hilo : hilosReponedores)
            try {
                hilo.join();
            } catch (InterruptedException ex) {
                // Capturar la excepción y no hacer nada
                // esta finalizando la ejecución
            }
        
        try {
            // Indica al hilo principal que ha finalizado
            this.finEjecucion.await();
        } catch (InterruptedException | BrokenBarrierException ex) {
            // Capturar la excepción y no hacer nada
            // esta finalizando la ejecución
        }
    }
    
    private void reponerProducto() throws InterruptedException, BrokenBarrierException {
        // Autoriza a los reponedores a comenzar y restaura el punto de sincronización
        this.iniciaReposicion.await();
        this.iniciaReposicion.reset();
        
        // Espera hasta que hayan concluido la reposición y restaura el punto de sincronización
        this.finReposicion.await();
        this.finReposicion.reset();
    }
    
    /**
     * Devuelve un producto diferente, si es posible, de la lista de productos
     * disponibles.
     * @return 
     */
    private Optional<TipoProducto> obtenerProducto() {
        Optional<TipoProducto> producto = Optional.empty();
        int productoRevisado = PRIMERO;
            
        while( (productoRevisado < productos.length) && !producto.isPresent() ) {
            TipoProducto tipoProducto = productos[ultimoProducto];
            if( listaProductos[ultimoProducto] > NO_HAY_PRODUCTO ) {
                listaProductos[ultimoProducto]--;
                producto = Optional.ofNullable(tipoProducto);
            } 
            
            ultimoProducto = (ultimoProducto + 1) % productos.length;
            productoRevisado++;
        }
        
        return producto;
    }
    
    /**
     * Asignamos el producto al primer pedido disponible comenzando desde el último
     * que fue asignado
     * de esta operación. 
     * @param producto
     * @return resultado de la acción si ha podido realizar o no
     * @throws InterruptedException 
     */
    private boolean asignarProducto(TipoProducto producto) throws InterruptedException {
        int tiempoProcesado;
        int numPedido = PRIMERO;
        boolean asignado = !ASIGNADO;
        
        // Comprobamos si han solicitado la interrupción
        if( Thread.interrupted() )
            throw new InterruptedException();
        
        // Asignamos el producto al primer pedido posible
        while( (numPedido < resultadoEjecucion.size()) && !resultadoEjecucion.isEmpty()
                && !asignado ) { 
            Pedido pedido = resultadoEjecucion.get(indicePedido);
            if( pedido.addProducto(producto) ) {
                System.out.println("El HILO(" + iD + ") ha asignado el producto " +
                                    producto + " al pedido " + pedido.getID());
                asignado = ASIGNADO;   
            }   
            
            indicePedido = (indicePedido + 1) % resultadoEjecucion.size();
            numPedido++;
        }
        
        // Se simula un tiempo de procesamiento
        tiempoProcesado = MIN_TIEMPO_PROCESADO + aleatorio.nextInt(VARIACION_TIEMPO_PROCESADO);
        TimeUnit.SECONDS.sleep(tiempoProcesado);
        
        return asignado;
    }
    
    /**
     * Se obtiene una persona de la lista compartida entre los gestores para crear
     * un nuevo pedido donde asignar el producto. Si no ha persona se indica que ha
     * finalizado la ejecución del gestor.
     * @param producto
     * @return si ha finalizado el gestor o no.
     */
    private boolean nuevoPedido(TipoProducto producto) {
        boolean terminaEjecucion = !FIN_EJECUCION;
        Optional<Persona> persona;
        Pedido pedido;
        
        try {
            persona = Optional.ofNullable(listaPersonas.remove(PRIMERO));
        } catch(IndexOutOfBoundsException ex) {
            // No hay personas en la lista
            persona = Optional.empty();
        }
        
        if( persona.isPresent() ) {
            pedido = new Pedido("Pedido-" + resultadoEjecucion.size(), persona.get());
            pedido.addProducto(producto);
            resultadoEjecucion.add(pedido);
        
            System.out.println("El HILO(" + iD + ") ha creado un pedido " + pedido.getID() + 
                               " para " + persona.get() + " y le ha asignado el producto " + producto);
        } 
        
        if( (resultadoEjecucion.size() > numPedidos) || !persona.isPresent() )
            terminaEjecucion = FIN_EJECUCION;
        
        return terminaEjecucion;
    }
}
