/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2122.problemassesion3.grupo3;

import static es.uja.ssccdd.curso2122.problemassesion3.grupo3.Constantes.MIN_PROVEEDORES;
import static es.uja.ssccdd.curso2122.problemassesion3.grupo3.Constantes.OBJETIVO_FINALIZACION;
import static es.uja.ssccdd.curso2122.problemassesion3.grupo3.Constantes.VARIACION_PROVEEDORES;
import static es.uja.ssccdd.curso2122.problemassesion3.grupo3.Constantes.aleatorio;
import java.util.ArrayList;
import java.util.concurrent.Phaser;

/**
 *
 * @author UJA
 */
public class Sesion3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException {
        Thread[] listaHilos;
        Proveedor proveedor;
        ArrayList<Ordenador>[] listaResultados;
        Phaser finEjecucion;
        int numProveedores;
        int finalizarProveedores;
        
        // Ejecución del hilo principal
        System.out.println("Ha iniciado la ejecución el Hilo(PRINCIPAL)");
        
        // Inicialización de variables
        numProveedores = MIN_PROVEEDORES + aleatorio.nextInt(VARIACION_PROVEEDORES);
        finalizarProveedores = (numProveedores * OBJETIVO_FINALIZACION) / 100;
        finEjecucion = new Phaser(finalizarProveedores + 1);
        listaResultados = new ArrayList[numProveedores];
        listaHilos = new Thread[numProveedores];
        for(int i = 0; i < numProveedores; i++) {
            listaResultados[i] = new ArrayList();
            proveedor = new Proveedor("Proveedor-" + i, listaResultados[i], finEjecucion);
            listaHilos[i] = new Thread(proveedor);
        }
        
        // Cuerpo de ejecución del hilo principal
        for(Thread hilo : listaHilos)
            hilo.start();
        
        System.out.println("Hilo(PRINCIPAL) espera a que finalicen " + finalizarProveedores + " proveedores");
        finEjecucion.arriveAndAwaitAdvance();
        
        System.out.println("Hilo(PRINCIPAL) solicita la INTERRUPCION de los proveedores y espera a su finalización");
        for(Thread hilo : listaHilos)
            hilo.interrupt();
        
        for(Thread hilo : listaHilos)
            hilo.join();
        
        // Presentar resultados
        for(int i = 0; i < numProveedores; i++) {
            System.out.println("_______ Resultados Proveedor " + i + " _____________");
            for(Ordenador ordenador : listaResultados[i])
                System.out.println(ordenador);
            System.out.println("_________________________________________________");
        }
        
        // Finalización
        System.out.println("Ha finalizado la ejecución el Hilo(PRINCIPAL)");
    }  
}
