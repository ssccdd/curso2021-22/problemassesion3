/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2122.problemassesion3.grupo3;

import static es.uja.ssccdd.curso2122.problemassesion3.grupo3.Constantes.ASIGNADO;
import static es.uja.ssccdd.curso2122.problemassesion3.grupo3.Constantes.FABRICACION_NECESARIA;
import static es.uja.ssccdd.curso2122.problemassesion3.grupo3.Constantes.FINAL_PEDIDO;
import static es.uja.ssccdd.curso2122.problemassesion3.grupo3.Constantes.MIN_ORDENADORES;
import static es.uja.ssccdd.curso2122.problemassesion3.grupo3.Constantes.MIN_TIEMPO_PROCESADO;
import static es.uja.ssccdd.curso2122.problemassesion3.grupo3.Constantes.OBJETIVO_CONSTRUCCION;
import static es.uja.ssccdd.curso2122.problemassesion3.grupo3.Constantes.PRIMERO;
import es.uja.ssccdd.curso2122.problemassesion3.grupo3.Constantes.TipoComponente;
import static es.uja.ssccdd.curso2122.problemassesion3.grupo3.Constantes.VARIACION_ORDENADORES;
import static es.uja.ssccdd.curso2122.problemassesion3.grupo3.Constantes.VARIACION_TIEMPO_PROCESADO;
import static es.uja.ssccdd.curso2122.problemassesion3.grupo3.Constantes.aleatorio;
import static es.uja.ssccdd.curso2122.problemassesion3.grupo3.Constantes.componentes;
import java.util.ArrayList;
import java.util.Optional;
import java.util.concurrent.Phaser;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pedroj
 */
public class Proveedor implements Runnable {
    private final String iD;
    private final ArrayList<Ordenador> resultadoEjecucion;
    private final Phaser finEjecucion;
    private final ArrayList<Componente> listaComponentes;
    private final Phaser finFabricacion;
    private final boolean[] fabricacionPendiente;
    private int numFabricante;
    private Thread[] hilosFabricantes;
    private int ordenadoresCompletados;

    public Proveedor(String iD, ArrayList<Ordenador> resultadoEjecucion, Phaser finEjecucion) {
        this.iD = iD;
        this.resultadoEjecucion = resultadoEjecucion;
        this.finEjecucion = finEjecucion;
        
        // Intercambio de componentes con sus fabricantes
        this.listaComponentes = new ArrayList();
        
        // Comonentes pendientes de fabricación
        this.hilosFabricantes = new Thread[componentes.length];
        this.numFabricante = 0;
        this.fabricacionPendiente = new boolean[componentes.length];
        for(int i = 0; i < componentes.length; i++) 
            fabricacionPendiente[i] = FABRICACION_NECESARIA;
        
        // Crea el elemento de sincronización con sus fabricantes y 
        // se registra en el
        this.finFabricacion = new Phaser();
        this.finFabricacion.register();
        
        // Valor de ordenadores que deben completarse
        this.ordenadoresCompletados = MIN_ORDENADORES + aleatorio.nextInt(VARIACION_ORDENADORES);
        this.ordenadoresCompletados = (ordenadoresCompletados * OBJETIVO_CONSTRUCCION) / 100; 
    }

    
    @Override
    public void run() {
        Optional<Componente> componente;
    
        
        System.out.println("El HILO(" + iD + ") comienza la preparación de " +
                           ordenadoresCompletados + " ordenadores");
        
        try {
            crearOrdenadores();
            
            // Hasta su finalización o su interrupción
            while( ordenadoresCompletados != FINAL_PEDIDO ) {
                
                nuevosComponentes();
                
                do {
                    
                    componente = obtenerComponente();
                    
                    if( componente.isPresent() ) {
                        // Comprobamos que tenemos un componente
                        asignarComponente(componente.get());
                        System.out.println("El HILO(" + iD + ") componentes pendientes " +
                                listaComponentes.size());
                    }
                            
                } while( componente.isPresent() );
                
            }
            
            
        } catch(InterruptedException ex) {
            System.out.println("El HILO(" + iD + ") ha sido INTERRUMPIDO");
        } finally {
            finEjecucion.arriveAndDeregister();
        }
    }

    public String getiD() {
        return iD;
    }
    
    private void crearOrdenadores() {
        
        for(int i = 0; i < ordenadoresCompletados; i++) {
            Ordenador ordenador = new Ordenador(iD + "-Ordenador-" + i);
            resultadoEjecucion.add(ordenador);
        }
    }
    
    /**
     * Crea tantos fabricantes como tipos de componentes falten para completar
     * los ordenadores. Esperará hasta que estén todos fabricados.
     * @throws InterruptedException 
     */
    private void nuevosComponentes() throws InterruptedException {
        listaComponentes.clear();
        
        for(int i = 0; i < fabricacionPendiente.length; i++) {
            TipoComponente tipoComponente = componentes[i];
            if ( fabricacionPendiente[i] ) {
                Fabricante fabricante = new Fabricante(iD + "-" + tipoComponente + "-" + numFabricante,
                                                  listaComponentes, tipoComponente, finFabricacion);
                hilosFabricantes[i] = new Thread(fabricante);
                hilosFabricantes[i].start();
            }
        }
        
        // Espera a que finalicen con la fabricación
        finFabricacion.awaitAdvanceInterruptibly(finFabricacion.arrive());
    }
    
    /**
     * Optiene un componente si está presente en la lista de componentes 
     * compartida entre los proveedores.
     * Solo se permite la interrupción si no hemos obrenido un componente.
     * @return el componente si se encuentra presente en la lista
     * @throws InterruptedException 
     */
    private Optional<Componente> obtenerComponente() throws InterruptedException {
        Optional<Componente> componente;
        int tiempo;
        
        // Simulamos el tiempo de operación
        tiempo = MIN_TIEMPO_PROCESADO + aleatorio.nextInt(VARIACION_TIEMPO_PROCESADO);
        TimeUnit.SECONDS.sleep(tiempo);
        
        try {
            componente = Optional.ofNullable(listaComponentes.remove(PRIMERO));
        } catch(IndexOutOfBoundsException ex) {
            // No hay componente
            componente = Optional.empty();
        }
        
        return componente;
    }
    
    /**
     * Asigna un componente a la lista de ordenadores disponibles si es posible
     * @param componente 
     */
     
    private void asignarComponente(Componente componente) {
        boolean asignado = !ASIGNADO;
        int indice = PRIMERO;
        
        // Asignamos el componente al primer ordenador posible
        while( (indice < resultadoEjecucion.size()) && !asignado ) { 
            Ordenador ordenador = resultadoEjecucion.get(indice);
            if( ordenador.addComponente(componente) ) {
                asignado = ASIGNADO;
                if( ordenador.isCompleto() )
                    // Uno menos para finalizar el trabajo
                    ordenadoresCompletados--;
                
                System.out.println("El HILO(" + iD + ") ha asignado " + componente +
                                   "al ordenador " + ordenador.getID() + 
                                   " ordenadores pendientes " + ordenadoresCompletados);
            }
            
            
            indice++;
        }
            
        // No se necesita más ese componente
        if( !asignado ) {
            fabricacionPendiente[componente.getTipo().ordinal()] = !FABRICACION_NECESARIA; 
        }
    }
}
