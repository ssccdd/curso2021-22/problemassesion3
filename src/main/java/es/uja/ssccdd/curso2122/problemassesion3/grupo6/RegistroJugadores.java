/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package es.uja.ssccdd.curso2122.problemassesion3.grupo6;

import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;
import static es.uja.ssccdd.curso2122.problemassesion3.grupo6.Utils.*;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class RegistroJugadores implements Runnable {

    private final int id;
    private final ArrayList<Jugador> jugadoresCreados;
    private final CountDownLatch sincCreacionJugadores;

    public RegistroJugadores(int id, ArrayList<Jugador> jugadoresCreados, CountDownLatch sincCreacionJugadores) {
        this.id = id;
        this.jugadoresCreados = jugadoresCreados;
        this.sincCreacionJugadores = sincCreacionJugadores;
    }

    @Override
    public void run() {

        int identificador = 0;
        System.out.println(Thread.currentThread().getName() + " ha comenzado.");

        for (int i = 0; i < JUGADORES_A_GENERAR; i++) {

            try {
                int tiempoEspera = random.nextInt(TIEMPO_CREACION_JUGADORES_MAX - TIEMPO_CREACION_JUGADORES_MIN) + TIEMPO_CREACION_JUGADORES_MIN;
                TimeUnit.MILLISECONDS.sleep(tiempoEspera);

                int aleatorioJugador = random.nextInt(VALOR_GENERACION);
                jugadoresCreados.add(new Jugador(identificador++, RolJugador.getRolJugador(aleatorioJugador)));

            } catch (InterruptedException ex) {
                //Este hilo no tiene que ser interrumpido por lo que no hace nada si es interrumpido.
            } finally {
                sincCreacionJugadores.countDown();
            }
        }

    }
    
        /**
     * Devuelve una cadena con la información de la clase
     *
     * @return Cadena de texto con la información
     */
    @Override
    public String toString() {
        StringBuilder mensaje = new StringBuilder();
        mensaje.append(Thread.currentThread().getName()).append(" ");

        mensaje.append("\n Jugadores registrados:  ").append(jugadoresCreados.size()).append(": ");

        for (Jugador jugador : jugadoresCreados) {
            mensaje.append("\n\t").append(jugador.toString());
        }

        return mensaje.toString();
    }

}
